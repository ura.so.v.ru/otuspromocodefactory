﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public void Add(T model)
        {
            List<T> list = Data.ToList();
            list.Add(model);
            Data = list;
        }

        public async Task<T> Delete(Guid id)
        {
            List<T> list = Data.ToList();
            var model = list.FirstOrDefault(x => x.Id == id);
            list.Remove(model);
            Data = list;
            return await Task.FromResult(model);
        }

        public Task<T> UpdateEntity(T entity)
        {
            var dto = Data.FirstOrDefault(x => x.Id == entity.Id);
            List<T> list = Data.ToList();
            list.Remove(dto);
            dto = entity;
            list.Add(dto);
            Data = list;
            return Task.FromResult(dto);
        }
    }
}