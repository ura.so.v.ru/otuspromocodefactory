﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mapper
{
    public static class MapEntity
    {
        public static Employee CreateEmployeeRequestInEmployee(CreateEmployeeRequest modelVM)
        {
            return new Employee
            {
                Id = Guid.NewGuid(),
                FirstName = modelVM.FirstName,
                LastName = modelVM.LastName,
                Email = modelVM.Email,
                AppliedPromocodesCount = modelVM.AppliedPromocodesCount,
                Roles = modelVM.Roles.Select(x => RoleItemResponseInRole(x)).ToList()
            };
        }
        public static Role RoleItemResponseInRole(RoleItemResponse modelVM)
        {
            return new Role
            {
                Id = modelVM.Id,
                Name = modelVM.Name,
                Description = modelVM.Description
            };
        }
        public static RoleItemResponse RoleInRoleItemResponse(Role dto)
        {
            return new RoleItemResponse
            {
                Id = dto.Id,
                Name = dto.Name,
                Description = dto.Description
            };
        }
        public static EmployeeResponse EmployeeInEmployeeResponse(Employee dto)
        {
            return new EmployeeResponse
            {
                Id = dto.Id,
                FullName = dto.FullName,
                Email = dto.Email,
                AppliedPromocodesCount = dto.AppliedPromocodesCount,
                Roles = dto.Roles.Select(x => RoleInRoleItemResponse(x)).ToList()
            };
        }
        public static EmployeeShortResponse EmployeeInEmployeeShortResponse(Employee dto)
        {
            return new EmployeeShortResponse
            {
                Id = dto.Id,
                Email = dto.Email,
                FullName = dto.FullName
            };
        }
    }
}
