﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.WebHost.Mapper;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                MapEntity.EmployeeInEmployeeShortResponse(x)).ToList();

            return employeesModelList;
        }
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var repsonse = MapEntity.EmployeeInEmployeeResponse(employee);
            return repsonse;
        }
        /// <summary>
        /// Удалить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<EmployeeShortResponse>> DeleteEmployee(Guid id)
        {
            var model = await _employeeRepository.Delete(id);
            var employeeShortResponse = MapEntity.EmployeeInEmployeeShortResponse(model);
            return await Task.FromResult(employeeShortResponse);
        }
        /// <summary>
        /// Добавить нового сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<EmployeeResponse>> Add(CreateEmployeeRequest model)
        {
            var employee = MapEntity.CreateEmployeeRequestInEmployee(model);

            _employeeRepository.Add(employee);

            var response = MapEntity.EmployeeInEmployeeResponse(employee);
            return await Task.FromResult(response);
        }
        /// <summary>
        /// Обновить данные сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult<EmployeeShortResponse>> Edit(Guid id ,CreateEmployeeRequest model)
        {
            var employeeModel = MapEntity.CreateEmployeeRequestInEmployee(model);
            employeeModel.Id = id;

            var employeeDto = await _employeeRepository.UpdateEntity(employeeModel);

            var response = MapEntity.EmployeeInEmployeeShortResponse(employeeDto);
            
            return await Task.FromResult(response);
        }
    }
}